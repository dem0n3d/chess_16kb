import '../node_modules/jquery-ui/themes/base/all.css';
import './style.css';
import './board.css';

import $ from 'jquery';
import guid from 'uuid/v4';
import {Chess} from 'chess.js';
import axios from 'axios';
import React from 'react';
import ReactDOM from 'react-dom';
import range from 'lodash/range';

import {GameContext} from "./contexts";


const types = {
    r: '♜',
    n: '♞',
    b: '♝',
    q: '♛',
    k: '♚',
    p: '♟'
};

export const Figure = ({type, color}) => (
    <div className={`figure ${color[0] == "b" ? "black" : "white"}`}>
        {types[type]}
    </div>
);


export class Board extends React.Component {

    render() {
        var chess = new Chess(this.props.fen);
        const figures = chess.board();

        return (
            <table>
                <tbody>
                {range(8).map(i => (
                    <tr key={i}>
                        <td>
                            {8 - i}
                        </td>
                        {range(8).map(j => (
                            <td className="cell" key={j}>
                                {figures[i][j] && (
                                    <Figure type={figures[i][j].type} color={figures[i][j].color}/>
                                )}
                            </td>
                        ))}
                    </tr>
                ))}
                <tr>
                    <td></td>
                    {range(8).map(j => (
                        <td key={j}>
                            {"abcdefgh"[j]}
                        </td>
                    ))}
                </tr>
                </tbody>
            </table>
        );
    }

}


export const BoardContainer = (props) => (
    <GameContext.Consumer>
        {val => <Board fen={val.fen} />}
    </GameContext.Consumer>
);
